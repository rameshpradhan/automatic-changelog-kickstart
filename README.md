# Demo Repository for automatic Changelog-Generation and Versioning with GIT

More details can be found in the following blog post:

[Automatic Changelog and Versioning with GIT (English)](https://www.philipp-doblhofer.at/en/blog/automatic-changelog-and-versioning-with-git/)\
[Automatischer Changelog und Versionierung mit GIT (Deutsch)](https://www.philipp-doblhofer.at/blog/automatischer-changelog-und-versionierung-mit-git/)
